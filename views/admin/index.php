<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
?>
<div class="author-index">

<h1><?= Html::encode($this->title) ?></h1>
<div class="container">
    <div class="d-flex ">
        <p class="col-md-6">
            <?= Html::a('Create Author', ['author/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <p class="col-md-6">
            <?= Html::a('Create Book', ['book/create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
    <div class="d-flex ">
        <div class="col-md-6">
            
            <?= GridView::widget([
                'dataProvider' => $authorProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    
                    'id',
                    'name',
                    'biography:ntext',
                    [
                        'attribute' => 'numBooks',
                    
                        'value'=>function($model)
                        {
                            return count($model->books);
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'urlCreator' => function ($action, $model, $key, $index) 
                        {
                            
                            if ($action === 'update')
                            {
                                return Url::to(['author/update', 'id' => $model->id]);
                            }
                            elseif($action === 'view')
                            {
                                return Url::to(['author/view', 'id' => $model->id]);
                            }
                            else
                            {
                                return Url::to(['author/delete', 'id' => $model->id]);
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            
            <?= GridView::widget([
                'dataProvider' => $bookProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'name',
                    [
                        'attribute' => 'author',
                        'label' => 'author',
                        'value'=>'author0.name'
                    ],
                    'description',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'urlCreator' => function ($action, $model, $key, $index) 
                        {
                            if ($action === 'update')
                            {
                                return Url::to(['book/update', 'id' => $model->id]);
                            }
                            elseif($action === 'view')
                            {
                                return Url::to(['book/view', 'id' => $model->id]);
                            }
                            else
                            {
                                return Url::to(['book/delete', 'id' => $model->id]);
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>






</div>