<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%author}}`
 */
class m211218_182231_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'author' => $this->integer()->notNull(),
        ]);

        // creates index for column `author`
        $this->createIndex(
            '{{%idx-books-author}}',
            '{{%books}}',
            'author'
        );

        // add foreign key for table `{{%author}}`
        $this->addForeignKey(
            '{{%fk-books-author}}',
            '{{%books}}',
            'author',
            '{{%author}}',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%author}}`
        $this->dropForeignKey(
            '{{%fk-books-author}}',
            '{{%books}}'
        );

        // drops index for column `author`
        $this->dropIndex(
            '{{%idx-books-author}}',
            '{{%books}}'
        );

        $this->dropTable('{{%books}}');
    }
}
